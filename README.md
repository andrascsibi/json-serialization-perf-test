# Node.js JSON serialization performance tests

Here's a little test that tries to write big Javascript objects (aka. BFOs) to files. Run it!

    time node test.js

Here are the results on my machine (node version: v0.10.16, ubuntu 12.04)

    0 s, 1.632 ms - done reading SFO
    0 s, 0.106 ms - starting test: 50000
    2 s, 594.240 ms - made it big
    1 s, 398.156 ms - stringified BF Array 50000
    0 s, 315.666 ms - wrote BF Array 50000
    1 s, 509.336 ms - stringified BF Object 50000
    0 s, 327.000 ms - wrote BF Object 50000
    0 s, 0.004 ms - starting test: 100000
    5 s, 617.891 ms - made it big
    3 s, 279.638 ms - stringified BF Array 100000
    0 s, 619.595 ms - wrote BF Array 100000
    3 s, 356.973 ms - stringified BF Object 100000
    0 s, 622.881 ms - wrote BF Object 100000
    0 s, 0.008 ms - starting test: 200000
    11 s, 108.947 ms - made it big
    96 s, 560.316 ms - stringified BF Array 200000
    1 s, 234.379 ms - wrote BF Array 200000
    269 s, 395.781 ms - stringified BF Object 200000
    1 s, 344.096 ms - wrote BF Object 200000

    real  6m39.463s
    user  6m35.449s
    sys 0m6.164s

## Performance

As you can see, things doesn't seem to be linear.

|type|records|file size|time|
|----|-------|---------|----|
|Array| 50000 |  40M | 1 s, 398.156 ms |
|Object| 50000 |  41M | 1 s, 509.336 ms |
|Array| 100000 |  79M | 3 s, 279.638 ms |
|Object| 100000 |  81M | 3 s, 356.973 ms |
|Array| 200000 | 161M | 96 s, 560.316 ms |
|Object | 200000 | 158M | 269 s, 395.781 ms |

