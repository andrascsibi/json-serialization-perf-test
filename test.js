
var fs = require('fs');
var deepcopy = require('deepcopy');
var file = __dirname + '/sfo.json';

var start = process.hrtime();

var elapsed_time = function(note){
    var precision = 3; // 3 decimal places
    var elapsed = process.hrtime(start)[1] / 1000000; // divide by a million to get nano to milli
    console.log(process.hrtime(start)[0] + " s, " + elapsed.toFixed(precision) + " ms - " + note); // print message + time
    start = process.hrtime(); // reset the timer
};

fs.readFile(file, 'utf8', function (err, data) {
  if (err) {
    console.log('Error: ' + err);
    return;
  }

  elapsed_time("done reading SFO");
  data = JSON.parse(data);


  var test = function(n) {
    var bfo = {};
    var bfa = [];

    elapsed_time("starting test: " + n);

    for (var i = 0; i < n; ++i) {
      bfo["foo_" + i] = deepcopy(data);
      bfa.push(deepcopy(data));
    }

    elapsed_time("made it big");

    var bfaStr = JSON.stringify(bfa);
    elapsed_time("stringified BF Array " + n);

    fs.writeFileSync("bfa_" + n + ".json", bfaStr);
    elapsed_time("wrote BF Array " + n);

    var bfoStr = JSON.stringify(bfo);
    elapsed_time("stringified BF Object " + n);

    fs.writeFileSync("bfo_" + n + ".json", bfoStr);
    elapsed_time("wrote BF Object " + n);

  };

  test(50000);
  test(100000);
  test(200000);
//  test(300000);

});
